import pygame as pg
import Hasi
import Wal
import Hackerman
import Pacman
import RainbowCycle
from matrixclient import PGMatrixApp
import os
import secrets
import paho.mqtt.client as mqtt


MqttMsgExist = False
MqttMsgData=""
FontSheet = []
FontSheet.append("/home/pi/info-system-ii/Python/images/Hackerman37x49ws.png")
FontSheet.append("/home/pi/info-system-ii/Python/images/Hackerman37x49.png")
FontSheet.append("/home/pi/info-system-ii/Python/images/Hackerman37x49_WhiteBackBlackFont.png")
PacmanSheet = ["/home/pi/info-system-ii/Python/images/pacmanEyeAlphaGreen32.png"]
WeihHasiSheet =["/home/pi/info-system-ii/Python/images/WeihnHase1.png"]
WalSheet =["/home/pi/info-system-ii/Python/images/SpriteSheet.PNG"]

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.connected_flag=True #set flag


def on_message(client, Data1, msg):
    print(msg.topic + " " + str(msg.qos) + " " + str(msg.payload))
    global MqttMsgData 
    global MqttMsgExist
    MqttMsgData=msg.payload
    MqttMsgExist=True


def on_publish(client, mqttc, obj, mid):
    print("mid: " + str(mid))


def on_subscribe(client, mqttc, obj, mid, granted_qos):
    print("Subscribed: " + str(mid) + " " + str(granted_qos))


def on_log(client, mqttc, obj, level, string):
    print(string)


class DemoImage(PGMatrixApp):
    def __init__(self):
        super().__init__()
        self.hack = Hackerman.hackerman(FontSheet, 37, 49, 10, 10)
        # self.Pac16 = Pacman.pacman(PacmanSheet, 16, 16, 14, 14)
        self.Pac32 = Pacman.pacman(PacmanSheet, 32, 32, 3, 1)
        self.hasi = Hasi.WeihHasi(WeihHasiSheet, 32, 32,10, 4)
        self.wal = Wal.Wal(WalSheet, 32, 32, 10, 2)
        self.Image = [self.hack.get_text_image("   ", -13, False, 0, 1),
                      self.hack.get_text_image("   ", -13, False, 0, 1)]
        self.mode = 99
        self.Rainbow = RainbowCycle.neopixel()
        self.lastImage = pg.Surface((512, 32))
        print("init1")

    def setup(self):
        print(str(self.width))

    def logic_loop(self):
        pass

    def graphics_loop(self):
      
        global MqttMsgExist
        if MqttMsgExist:
            print(MqttMsgData)
            self.mode=int(MqttMsgData)
            
            MqttMsgExist=False
        if self.mode == 10:
            pos = [0, 270]
            self.Image[0] = self.hack.animation(pos)
            if self.Image[0]:
                self.lastImage.blit(self.Image[0], (0, 0))
                self.screen.blit(self.lastImage, (0, 0))

            else:
                self.mode = 1
               

        if self.mode == 1:

            self.Image[0] = self.Pac32.animation_left_clean(self.lastImage)
            if self.Image[0]:
                self.screen.blit(self.Image[0], (0, 0))
            else:
                self.mode = 99

        if self.mode == 2:

            self.Image[0] = self.hasi.animation_left(self.lastImage)
            if self.Image[0]:
                self.screen.blit(self.Image[0], (0, 0))
            else:
                self.mode = 99

        if self.mode == 3:

            self.Image[0] = self.wal.animation_left(self.lastImage)
            if self.Image[0]:
                self.screen.blit(self.Image[0], (0, 0))
            else:
                self.mode = 99

        if self.mode == 98:
            self.Image[0] = self.Rainbow.rainbow_cycle()
            if self.Image[0]:
                self.screen.blit(self.Image[0], (0, 0))

if __name__ == '__main__':
    hostname = secrets.MqttServer #example
    response = 1
    while response != 0:
       
        response = os.system("ping -c 1 " + hostname)

        #and then check the response...
        if response != 0:
            print(hostname, 'is Down!…')
    # Create NeoPixel object with appropriate configuration.
    
    # Intialize the library (must be called once before other functions).
    
    mqtt.Client.connected_flag=False#create flag in class
    client = mqtt.Client()
    client.username_pw_set(secrets.user,secrets.pw)
    client.on_connect = on_connect
    client.on_message = on_message
    
    client.connect(secrets.MqttServer, 1883, 60)

    
    client.subscribe("InfoSystem02", 0)
    client.loop_start()
    
    MqttMsgExist = False
    
    DemoImage().run()
