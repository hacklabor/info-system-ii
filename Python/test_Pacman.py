from unittest import TestCase

from Hackerman import hackerman

testImage = ["images/TestBW_32px_10x10.png"]


class Test_Hackerman(TestCase):
    def test_get_text_image(self):
        hack = hackerman(testImage, 25, 32, 10, 10)
        img = hack.get_text_image("TESTTEXT", -5, False, 0, 0)
        self.assertEqual((25 - 5) * len("TESTTEXT"), img.get_width())

    def test_get_typeError(self):
        hack = hackerman(testImage, 25, 32, 10, 10)

        with self.assertRaises(TypeError):
            img = hack.get_text_image(1, 0)
        with self.assertRaises(TypeError):
            img = hack.get_text_image(hack, 0)
