import random

import pygame as pg

from Sprites import sprites


class hackerman:
    def __init__(self, sheet, size_x, size_y, x, y):
        self.letters = []

        for sh in sheet:
            spr = sprites(sh, size_x, size_y, x, y)
            self.letters.append(spr.get_all_images())
        self.lastImage = pg.Surface((512, 32))
        self.size_x = size_x
        self.size_y = size_y
        self.timer = 0
        self.cycle = 0
        self.LetterNr = -1
        self.trigger = 1

    def get_text_image(self, text, gap, flash, LetID, imageID):
        img = pg.Surface(((self.size_x + gap) * len(text), self.size_y))
        i = 0
        for letter in text:
            asc = ord(letter) - 32
            if i == LetID and flash is True:

                img.blit(self.letters[0][asc], (i * (self.size_x + gap), 0))
            else:
                img.blit(self.letters[imageID][asc], (i * (self.size_x + gap), 0))
            i += 1

        return img

    def build_image(self, posX, PosY, background: pg.Surface, image: pg.surface, color_key, alpha):
        """

        :param alpha: int
        :param color_key: Color
        :param image: pg.surface
        :type background: pg.Surface
        """
        image.set_colorkey(color_key)
        for x in posX:
            background.blit(image, (x, PosY))
        background.set_alpha(alpha)
        return background

    def animation(self, Pos):
        self.timer += 1

        if self.timer > self.trigger:
            background = pg.Surface((512, 32))
            if self.cycle == 0:
                if self.LetterNr == 8:
                    self.cycle += 1
                    self.trigger = 0
                else:
                    self.LetterNr += 1
                    img = self.get_text_image("HACKERMAN ", -15, True, self.LetterNr, 1)
                    self.lastImage = self.build_image(Pos, -5, background, img, (0, 0, 0), 100)
                    self.timer = 0
                    return self.lastImage
                self.timer = 0
            if self.cycle == 1:
                print(str(self.LetterNr))
                if self.LetterNr == -1:
                    self.cycle += 1
                    self.LetterNr = -1
                    self.trigger = 3
                else:
                    self.LetterNr -= 1
                    img = self.get_text_image("HACKERMAN ", -15, True, self.LetterNr, 1)
                    self.lastImage = self.build_image(Pos, -5, background, img, (0, 0, 0), 100)
                    self.timer = 0
                    return self.lastImage

                self.timer = 0
            if 1 < self.cycle < 40:
                for i in range(800):
                    x = random.randint(0, 511)
                    y = random.randint(0, 31)
                    background.set_at((x, y), (255, 255, 255))
                    #background.set_at((x + 1, y), (255, 255, 255))
                    #background.set_at((x - 1, y), (255, 255, 255))
                    #background.set_at((x, y + 1), (255, 255, 255))
                    #background.set_at((x, y - 1), (255, 255, 255))
                img = self.get_text_image("HACKERMAN ", -15, False, self.LetterNr, 1)
                self.lastImage = self.build_image(Pos, -5, background, img, (0, 0, 0), 100)
                self.cycle += 1
                self.timer = 0
                return self.lastImage
            self.cycle = 0
            self.trigger = 1
        else:
            return self.lastImage
