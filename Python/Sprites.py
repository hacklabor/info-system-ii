import pygame as pg
from PIL import Image


def get_all_images(spr):

    img = []
    for x in range(1, 11):
        for y in range(1, 11):
            img.append(spr.get_pygame_image(x, y))
    return img


class sprites:

    def __init__(self, sheet, sizeX, sizeY, x, y):
        self.__sheet = Image.open(sheet, mode="r")
        self.__XpixelSize = sizeX
        self.__YpixelSize = sizeY
        self.__spriteSize = (sizeX, sizeY)
        self.__xGrid = x
        self.__yGrid = y

    def __chk_xy(self, x, y):
        if x > self.__xGrid or x < 1:
            raise ValueError("x not in GridSize: 1-" + str(self.__xGrid))
        if y > self.__yGrid or y < 1:
            raise ValueError("y not in GridSize: 1-" + str(self.__yGrid))

    def get_image(self, x, y):

        self.__chk_xy(x, y)
        x0 = (x - 1) * self.__XpixelSize
        y0 = (y - 1) * self.__YpixelSize
        return self.__sheet.crop((x0, y0, x0 + self.__XpixelSize, y0 + self.__YpixelSize))

    def get_pygame_image(self, x, y):
        self.__chk_xy(x, y)
        image = self.get_image(x, y)
        return pg.image.fromstring(image.tobytes(), image.size, image.mode)

    def get_all_images(self):

        img = []
        for y in range(0, self.__yGrid):
            for x in range(0, self.__xGrid):
                img.append(self.get_pygame_image(x+1, y+1))
        return img

    def __del__(self):
        self.__sheet.close()
