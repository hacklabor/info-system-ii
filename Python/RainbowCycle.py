import pygame as pg


def wheel(WheelPos):
    WheelPos = 255 - WheelPos
    if WheelPos < 85:
        return 255 - WheelPos * 3, 0, WheelPos * 3

    if WheelPos < 170:
        WheelPos -= 85
        return 0, WheelPos * 3, 255 - WheelPos * 3
    WheelPos -= 170
    return WheelPos * 3, 255 - WheelPos * 3, 0


class neopixel:
    def __init__(self):
        pass
        self.rainbowC = 511 * 256

    def rainbow_cycle(self):
        img = pg.Surface((512, 32))
        for i in range(512):
            line = pg.Surface((1, 32))
            col = wheel((int(i * 256 / 512) + self.rainbowC) & 255)
            line.fill(col)
            img.blit(line, (511-i, 0))
        self.rainbowC -= 1
        if self.rainbowC == 0:
            self.rainbowC = 511 * 256
        return img
