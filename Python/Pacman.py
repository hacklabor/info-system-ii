import pygame as pg

from Sprites import sprites


class pacman:
    def __init__(self, sheet, size_x, size_y, x, y):
        self.pac = []

        for sh in sheet:
            spr = sprites(sh, size_x, size_y, x, y)
            self.pac.append(spr.get_all_images())
        self.lastImage = pg.Surface((512, 32))
        self.size_x = size_x
        self.size_y = size_y
        self.timer = 0
        self.cycle = 1
        self.SpriteNr = 0
        self.trigger = 0
        self.direction = 1

    def animation_left_clean(self, oldImage):
        self.timer += 1
        if self.timer > self.trigger:
            if self.cycle < 512 + 34:

                img = pg.Surface((self.cycle, 32))
                img.fill((0, 0, 0))
                print(int(self.SpriteNr))
                img.blit(self.pac[0][int(self.SpriteNr)], (self.cycle - 32, 0))
                img.set_colorkey((0, 255, 0))
                oldImage.blit(img, (0, 0))
                self.lastImage = oldImage
                self.cycle += 2
                self.SpriteNr += (.3*self.direction)
                if self.SpriteNr <= 0:
                    self.SpriteNr = 0
                    self.direction = self.direction * -1
                if self.SpriteNr > 2.8:
                    self.direction = self.direction*-1
                self.timer = 0
                return self.lastImage

            self.cycle = 0
        else:
            return self.lastImage
