from unittest import TestCase

import OpenWeatherMap
import secrets

testImage = ["images/TestBW_32px_10x10.png"]

OWM = OpenWeatherMap.OWMap(secrets.OWM_Api_KEY, secrets.OWM_Host)


class Test_OpenWeatherMap(TestCase):

    def test_get_request_1(self):
        state, value = OWM.get_request()
        self.assertEqual(200, state)

    def test_get_request_2(self):
        state, value = OWM.get_request()
        self.assertEqual("{", value[0])

    def test_get_data(self):
        res = OWM.get_data(60)

    def test_set_timezone_offset(self):
        OWM.set_timezone_offset(1000)
        res = OWM.get_timezone_offset()
        self.assertEqual(1000, res)
