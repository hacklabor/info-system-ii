from unittest import TestCase

from PIL import Image

from Sprites import sprites

testImage = ["images/TestBW_32px_10x10.png"]


class Testsprites(TestCase):
    def test_get_imageSize(self):
        img = Image.new('RGB', (32, 32))

        spr = sprites(testImage, 32, 32, 10, 10)
        img = spr.get_image(10, 10)
        self.assertEqual(32, img.width)
        self.assertEqual(32, img.height)
        spr.__del__()

    def test_get_Sprite_ColorWhite(self):
        img = Image.new('RGB', (32, 32))

        spr = sprites(testImage, 32, 32, 10, 10)
        img = spr.get_image(10, 10)
        col_left_up = img.getpixel((0, 0))
        col_right_down = img.getpixel((31, 31))
        self.assertEqual((255, 255, 255), col_left_up)
        self.assertEqual((255, 255, 255), col_right_down)
        spr.__del__()

    def test_get_Sprite_ColorBlack(self):
        img = Image.new('RGB', (32, 32))

        spr = sprites(testImage, 32, 32, 10, 10)
        img = spr.get_image(10, 9)
        col_left_up = img.getpixel((0, 0))
        col_right_down = img.getpixel((31, 31))

        self.assertEqual((0, 0, 0), col_left_up)
        self.assertEqual((0, 0, 0), col_right_down)
        spr.__del__()

    def test_get_ValueError(self):
        spr = sprites(testImage, 32, 32, 10, 10)
        with self.assertRaises(ValueError):
            spr.get_image(11, 10)
        with self.assertRaises(ValueError):
            spr.get_image(10, 11)
        with self.assertRaises(ValueError):
            spr.get_image(-1, 10)
        with self.assertRaises(ValueError):
            spr.get_image(10, -1)
        spr.__del__()

    def test_get_all_count(self):
        spr = sprites(testImage, 32, 32, 10, 10)
        pg_images = spr.get_all_images()
        self.assertEqual(100, len(pg_images))
        spr.__del__()



