#!/usr/bin/env python
from samplebase import SampleBase
from rgbmatrix import graphics
import time


class GraphicsTest(SampleBase):
    def __init__(self, *args, **kwargs):
        super(GraphicsTest, self).__init__(*args, **kwargs)

    def run(self):
        canvas = self.matrix
        font = graphics.Font()
        font.LoadFont("../../../fonts/7x13.bdf")
        red = graphics.Color(255, 0, 0)
        green = graphics.Color(0, 255, 0)
        black = graphics.Color(0, 0, 0)
        for z in range (0,10)

            for x in range(0,384):

                graphics.DrawLine(canvas, x-1, 0, x-1, 31, black)
                graphics.DrawLine(canvas, x, 0, x+15, 15, red)
                graphics.DrawLine(canvas, x, 31, x + 15, 16, red)
                time.sleep(0.01)




        graphics.DrawCircle(canvas, 15, 15, 10, green)

        blue = graphics.Color(0, 0, 255)
        graphics.DrawText(canvas, font, 2, 10, blue, "Text")

        time.sleep(10)   # show display for 10 seconds before exit


# Main function
if __name__ == "__main__":
    graphics_test = GraphicsTest()
    if (not graphics_test.process()):
        graphics_test.print_help()
