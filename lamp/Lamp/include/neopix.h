#pragma once
#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

class neopix
{
private:
    int helligkeit;
    long rainbowC = 0;
    uint32_t Wheel(Adafruit_NeoPixel& strip,byte WheelPos);
public:
     neopix();
    void rainbowCycle(Adafruit_NeoPixel& strip, int _Helligkeit);
    uint32_t colorRotate(Adafruit_NeoPixel& strip, int _Helligkeit);
   uint32_t  singleColor(Adafruit_NeoPixel& strip,uint32_t color, int _Helligkeit);
};






