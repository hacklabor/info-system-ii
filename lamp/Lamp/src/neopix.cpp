
#include <Arduino.h>
#include <neopix.h>
#include <Adafruit_NeoPixel.h>





neopix::neopix(){


}

// Slightly different, this makes the neopix equally distributed throughout
void neopix::rainbowCycle(Adafruit_NeoPixel& strip, int _Helligkeit) {
	uint16_t i;


	for (i = 0; i < strip.numPixels(); i++) {
		strip.setPixelColor(i,  Wheel(strip,(((i * 256 / strip.numPixels()) + rainbowC)) & 255));
		}

	rainbowC++;
	if (rainbowC >=  strip.numPixels() * 256)
	{
		rainbowC = 0;
	}
     strip.setBrightness(_Helligkeit);
     strip.show();   
  
}
uint32_t neopix::colorRotate(Adafruit_NeoPixel& strip, int _Helligkeit) {
	uint16_t i;
	uint32_t col =0 ;

	for (i = 0; i < strip.numPixels(); i++) {
		col= Wheel(strip,(((0* 256 / strip.numPixels()) + rainbowC)) & 255);
		strip.setPixelColor(i, col);
		}

	rainbowC++;
	if (rainbowC >=  strip.numPixels() * 256)
	{
		rainbowC = 0;


	}
     strip.setBrightness(_Helligkeit);
     strip.show();  
	 return  col;
   
}


uint32_t neopix:: singleColor(Adafruit_NeoPixel& strip,uint32_t color, int _Helligkeit)
{
  for (int i = 0; i < strip.numPixels(); i++)
  {
    strip.setPixelColor(i, color);
  }
  strip.setBrightness(_Helligkeit);
  strip.show();
 return color;
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t neopix::Wheel(Adafruit_NeoPixel& strip,byte WheelPos) {
	WheelPos = 255 - WheelPos;
	if (WheelPos < 85) {
		return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
	}
	if (WheelPos < 170) {
		WheelPos -= 85;
		return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
	}
	WheelPos -= 170;
	return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
}

