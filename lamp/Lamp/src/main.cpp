#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <secret.h>
#include <Adafruit_NeoPixel.h>
#include "SerialDebug.h"
#include "TimeTrigger.h"
#include "neopix.h"
#include <PubSubClient.h>

enum LED_MODE
{
  RAINBOW_CYLE = 0,
  SING_COLOR = 1,
  TOW_COLORS = 2,
  COLOR_ROTATE = 3,
  LED_OFF = 4,
  FROZEN = 5,
};

int brightness = 10;
boolean rainbowcolor = false;
boolean rainbowcycle = false;
boolean dreiercycle = false;
int speed = 50;
int mode = 0;
uint32_t color = 0;
int lastSpeed =0;
 uint32_t col=0;

// How many NeoPixels are attached to the Arduino?
Adafruit_NeoPixel strip(144, D2, NEO_RGB + NEO_KHZ800);
neopix neopixLED;
TimeTrigger _TriggerRainbow(100);
TimeTrigger _TriggerColor(500);
SerialDebug _deb(ALL);
WiFiClient espClient;
PubSubClient client(espClient);

// MQTT Last will and Testament
byte willQoS = 0;
const char *willTopic = "LampeIS/";
const char *willOnMessage = "online";
const char *willOffMessage = "offline";
const char *willClientID = "esp-LampeIS";
boolean willRetain = true;



uint32_t getColor(String value)
{
  int number = (int)strtol(&value[0], NULL, 16);

  int r = number >> 16;
  int g = number >> 8 & 0xFF;
  int b = number & 0xFF;
  return strip.Color(g, r, b);
}

void callback(char *topic, byte *payload, unsigned int length)
{
  enum key
  {
    SPEED = 0,
    BRIGHTNESS = 1,
    MODE = 2,
    COLOR = 3,
  };

  String command;
  int key;

  key = payload[0] - 48;
  for (unsigned int i = 1; i < length; i++)
  {
    command = command + String((char)payload[i]);
  }
  _deb.print("MQTT TOPIC", String(topic), 0);
  _deb.print("MQTT KEY", String(key), 0);
  _deb.print("MQTT VALUE", command, 0);

  switch (key)
  {
  case SPEED:
   
    speed = 300-(command.toInt()*3);
    _TriggerRainbow.setTimespann(speed);
    lastSpeed=speed;
    break;
  case BRIGHTNESS:
    brightness = command.toInt();
      _TriggerRainbow.setTimespann(5);
    break;
  case MODE:
    mode = command.toInt();
         _TriggerRainbow.setTimespann(speed);
    break;
  case COLOR:
    color = getColor(command);
     _TriggerRainbow.setTimespann(5);
    break;
  default:
    // statements
    break;
  }
}

void reconnect()
{
  // Loop until we're reconnected
  while (!client.connected())
  {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(willClientID, willTopic, willQoS, willRetain, willOffMessage))
    {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish(willTopic, willOnMessage, willRetain); // ... and resubscribe
      client.subscribe(mainTopic);
    }
    else
    {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 500 seconds before retrying
      delay(500);
    }
  }
}

void setup()
{
  Serial.begin(115200);
  Serial.println("Booting");
  wifi_station_set_hostname("esp_info_system");
  WiFi.mode(WIFI_STA);

  WiFi.begin("Hacklabor", "H4ckl4b0r");
  while (WiFi.waitForConnectResult() != WL_CONNECTED)
  {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]

  // No authentication by default
  ArduinoOTA.setPassword(OTAPWD);

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
    {
      type = "sketch";
    }
    else
    { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR)
    {
      Serial.println("Auth Failed");
    }
    else if (error == OTA_BEGIN_ERROR)
    {
      Serial.println("Begin Failed");
    }
    else if (error == OTA_CONNECT_ERROR)
    {
      Serial.println("Connect Failed");
    }
    else if (error == OTA_RECEIVE_ERROR)
    {
      Serial.println("Receive Failed");
    }
    else if (error == OTA_END_ERROR)
    {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  ArduinoOTA.handle();
  // Set BRIGHTNESS to about 1/5 (max = 255)
  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
                 // Turn OFF all pixels ASAP
  strip.setBrightness(brightness);
  strip.show();
  client.setServer(MqttServer, 1883);
  client.setCallback(callback);
  client.publish(SpeedTopic, "10");
}

// loop() function -- runs repeatedly as long as board is on ---------------

void loop()
{

  ArduinoOTA.handle();
  if (!client.connected())
  {
    reconnect();
  }
  client.loop();
 

  if (_TriggerRainbow.Trigger() & (mode == RAINBOW_CYLE))
  {    neopixLED.rainbowCycle(strip, brightness);
        }
  if (_TriggerRainbow.Trigger() & (mode == COLOR_ROTATE))
  {  col = neopixLED.colorRotate(strip, brightness);
 
    }
   if (_TriggerRainbow.Trigger() & (mode == OFF))
  {    neopixLED.singleColor(strip,0, brightness);
      _TriggerRainbow.setTimespann(1000);
  }
 if (_TriggerRainbow.Trigger() & (mode == SING_COLOR))
  {   
    col=  neopixLED.singleColor(strip,color, brightness);
         _TriggerRainbow.setTimespann(1000);
  }
   if (_TriggerColor.Trigger())
  {   
        
         //client.publish(ColorTopic, String(col).c_str());
      
  }
}